/**
 * A library wrapping EaselJS and Box2DWeb.
 * Contains support for keyboard input, 2D rendering and physics.
 * @constructor
 */
function Yarn() {}

/**
 * Access this object to see which keys are being held down.
 * @public
 */
Yarn.keysPressed = { left: false, right: false, up: false, down: false, spacebar: false };

/**
 * Must be called before using the Yarn library.
 * @public
 * @param {string} canvasID The canvas element that Yarn will render to.
 */
Yarn.init = function(canvasID) {

  // Initialize keyboard input

  $(document).keydown(function(e) {
    if (e.which == 37) { // left arrow
      e.preventDefault();
      Yarn.keysPressed.left = true;
    } else if (e.which == 38) { // up arrow
      Yarn.keysPressed.up = true;
    } else if (e.which == 39) { // right arrow
      e.preventDefault();
      Yarn.keysPressed.right = true;
    } else if (e.which == 40) { // down arrow
      e.preventDefault();
      Yarn.keysPressed.down = true;
    } else if (e.which == 32) { // spacebar
      e.preventDefault();
      Yarn.keysPressed.spacebar = true;
    } else {
      console.log(e.which);
    }
  });
  $(document).keyup(function(e) {
    if (e.which == 37) { // left arrow
      e.preventDefault();
      Yarn.keysPressed.left = false;
    } else if (e.which == 38) { // up arrow
      e.preventDefault();
      Yarn.keysPressed.up = false;
    } else if (e.which == 39) { // right arrow
      e.preventDefault();
      Yarn.keysPressed.right = false;
    } else if (e.which == 40) { // down arrow
      e.preventDefault();
      Yarn.keysPressed.down = false;
    } else if (e.which == 32) { // spacebar
      e.preventDefault();
      Yarn.keysPressed.spacebar = false;
    }
  });

  /**
   * The CreateJS global object
   * @private
   * @type {createjs.Stage}
   */
  Yarn.stage = new createjs.Stage(canvasID);

  createjs.Ticker.addEventListener('tick', Yarn.update);

  // Initialize Box2D stuff
  /**
   * The Box2D global object
   * @private
   * @type {Box2D.Dynamics.b2World}
   */
  Yarn.world = new Box2D.Dynamics.b2World(
    new Box2D.Common.Math.b2Vec2(0, 0),   // gravity
    true                                  // allow sleep
  );

  Yarn.lastTimestamp = Date.now();
};

/**
 * Set a function to be called on every update tick.
 * @public
 * @param {function} callback
 */
Yarn.addUpdateCallback = function(callback) {
  createjs.Ticker.addEventListener('tick', callback);
};

/**
 * @public
 */
Yarn.addEntity = function(entity) {
  Yarn.stage.addChild(entity.bitmap);
  Yarn.entities.push(entity);
};

Yarn.addHUDElement = function(element) {
  Yarn.stage.addChild(element.bitmap);
  Yarn.HUDElements.push(element);
};

/**
 * @public
 */
Yarn.toggleDebug = function() {
  if (Yarn.world.m_debugDraw) {
    console.log("Yarn Debug Off");
    Yarn.world.SetDebugDraw(null);
  } else {
    console.log("Yarn Debug On");
    var debugDraw = new Box2D.Dynamics.b2DebugDraw;
    debugDraw.SetSprite(document.getElementById('debugCanvas').getContext('2d'));
    debugDraw.SetDrawScale(Yarn.SCALE);
    debugDraw.SetFillAlpha(0.7);
    debugDraw.SetLineThickness(1.0);
    debugDraw.SetFlags(Box2D.Dynamics.b2DebugDraw.e_shapeBit | Box2D.Dynamics.b2DebugDraw.e_jointBit);
    //Yarn.world.SetDebugDraw(debugDraw);
  }
};

/**
 * Called every tick.
 * @private
 */
Yarn.update = function() {
  var now = Date.now();
  Yarn.fixedTimestepAccumulator += now - Yarn.lastTimestamp;
  Yarn.lastTimestamp = now;
  while(Yarn.fixedTimestepAccumulator >= Yarn.STEP) {

    // Update the camera
    Yarn.camera.velocity.x *= Yarn.camera.FRICTION;
    Yarn.camera.velocity.y *= Yarn.camera.FRICTION;
    Yarn.camera.position.x += Yarn.camera.velocity.x;
    Yarn.camera.position.y += Yarn.camera.velocity.y;
    Yarn.stage.setTransform(-Yarn.camera.position.x, -Yarn.camera.position.y);

    $.each(Yarn.HUDElements, function(i, element) {
      element.bitmap.x = element.position.x + Yarn.camera.position.x;
      element.bitmap.y = element.position.y + Yarn.camera.position.y;
    });

    $.each(Yarn.entities, function(i, entity) {
      if (entity.body) {
        // Synchronize Box2D and EaselJS positions
        entity.bitmap.rotation = entity.body.GetAngle() * (180  / Math.PI);
        entity.bitmap.x = entity.body.GetWorldCenter().x * Yarn.SCALE;
        entity.bitmap.y = entity.body.GetWorldCenter().y * Yarn.SCALE;
      }
    });
    Yarn.world.Step(Yarn.TIMESTEP, 10, 10);
    Yarn.fixedTimestepAccumulator -= Yarn.STEP;
    Yarn.stage.update();
    // not sure why these next two lines are necessary
    Yarn.world.ClearForces();
    if (Yarn.world.m_debugDraw) {
      Yarn.world.m_debugDraw.m_sprite.graphics.clear();
      Yarn.world.DrawDebugData();
    }
  }
};

/**
 * @private
 * @type {Array}
 */
Yarn.entities = [];
Yarn.HUDElements = [];

/**
 * @private
 */
Yarn.camera = {
  FRICTION: 0.9,
  position: {x: 0, y: 0},
  velocity: {x: 0, y: 0}
};

/**
 * Used to convert between EaselJS and Box2D coordinates.
 * @public
 * @type {number}
 */
Yarn.SCALE = 30;
Yarn.STEP = 20;
Yarn.TIMESTEP = 1 / Yarn.STEP;
Yarn.fixedTimestepAccumulator = 0;
Yarn.lastTimestamp = Date.now();

function YarnEntityBuilder() {
  // default parameters
  this.type = YarnEntityBuilder.PhysicsType.DYNAMIC;
  this.shape = YarnEntityBuilder.Shape.RECT;
  return this;
}

YarnEntityBuilder.PhysicsType = {NONE: 0, STATIC: 1, DYNAMIC: 2};
YarnEntityBuilder.Shape = {RECT: 0, CIRCLE: 1};

YarnEntityBuilder.prototype.setImage = function(image) { this.image = image; return this; };
YarnEntityBuilder.prototype.setPhysicsType = function(type) { this.type = type; return this; };
YarnEntityBuilder.prototype.setCircleShape = function(r) { this.radius = r; this.shape = YarnEntityBuilder.Shape.CIRCLE; return this; };
YarnEntityBuilder.prototype.setRectShape = function(w,h) { this.width = w; this.height = h; this.shape = YarnEntityBuilder.Shape.RECT; return this; };
YarnEntityBuilder.prototype.setInitialPosition = function(x,y) {this.x = x; this.y = y; return this; };

YarnEntityBuilder.prototype.finalize = function() {
  var entity = new YarnEntity();
  if (this.image) {
    entity.bitmap = new createjs.Bitmap(this.image);
    // TODO: calculate this from the dimensions of the image
    entity.bitmap.regX = entity.bitmap.regY = 32;
  }

  if (this.type != YarnEntityBuilder.PhysicsType.NONE) {
    entity.fixture = new Box2D.Dynamics.b2FixtureDef;
    // TODO: make customizable
    entity.fixture.density = 1;
    // TODO: make customizable
    entity.fixture.restitution = 0.6;

    if (this.shape == YarnEntityBuilder.Shape.CIRCLE) {
      // TODO: make radius customizable
      entity.fixture.shape = new Box2D.Collision.Shapes.b2CircleShape(this.radius / Yarn.SCALE);
    } else if (this.shape == YarnEntityBuilder.Shape.RECT) {
      // TODO: implement this
    }

    entity.bodyDef = new Box2D.Dynamics.b2BodyDef;
    if (this.type == YarnEntityBuilder.PhysicsType.DYNAMIC) {
      entity.bodyDef.type = Box2D.Dynamics.b2Body.b2_dynamicBody;
    } else {
      entity.bodyDef.type = Box2D.Dynamics.b2Body.b2_staticBody;
    }
    // TODO: make customizable
    entity.bodyDef.angularDamping = 0.7;
    // TODO: make customizable
    entity.bodyDef.linearDamping = 0.7;

    // TODO: don't assume bitmap
    // TODO: do we need to keep the body def?
    entity.bodyDef.position.x = entity.bitmap.x / Yarn.SCALE;
    entity.bodyDef.position.y = entity.bitmap.y / Yarn.SCALE;
    entity.body = Yarn.world.CreateBody(entity.bodyDef);
    entity.body.CreateFixture(entity.fixture);
  }

  Yarn.addEntity(entity);

  return entity;
};

/**
 * A class representing an independent 2D entity on the screen.
 * @public
 * @constructor
 */
function YarnEntity() {}

YarnEntity.ROTATION_SPEED = 1.5;
YarnEntity.MOVE_SPEED = 0.5;

/**
 * @public
 * @param w
 * @param h
 */
YarnEntity.prototype.makeRoundRect = function(w, h) {
  // TODO: move this to YarnEntityBuilder
  this.bitmap = new createjs.Shape(new createjs.Graphics().beginFill('#333').drawRoundRect(0, 0, w, h, 10));
  this.bitmap.regX = w / 2;
  this.bitmap.regY = h / 2;

  this.fixture = new Box2D.Dynamics.b2FixtureDef;
  this.fixture.density = 1;
  this.fixture.restitution = 0.6;
  this.fixture.shape = new Box2D.Collision.Shapes.b2PolygonShape();
  this.fixture.shape.SetAsBox((w / Yarn.SCALE)/2 , (h / Yarn.SCALE)/2);

  this.bodyDef = new Box2D.Dynamics.b2BodyDef;
  this.bodyDef.type = Box2D.Dynamics.b2Body.b2_staticBody;
  this.bodyDef.position.x = this.bitmap.x / Yarn.SCALE;
  this.bodyDef.position.y = this.bitmap.y / Yarn.SCALE;
  this.body = Yarn.world.CreateBody(this.bodyDef);
  this.body.CreateFixture(this.fixture);
};

/**
 * @public
 */
YarnEntity.prototype.rotateLeft = function() {
  this.body.ApplyTorque(-YarnEntity.ROTATION_SPEED);
};

/**
 * @public
 */
YarnEntity.prototype.rotateRight = function() {
  this.body.ApplyTorque(YarnEntity.ROTATION_SPEED);
};

/**
 * @public
 */
YarnEntity.prototype.moveForward = function() {
  this.body.ApplyImpulse(new Box2D.Common.Math.b2Vec2(
    Math.cos(this.body.GetAngle()) * YarnEntity.MOVE_SPEED,
    Math.sin(this.body.GetAngle()) * YarnEntity.MOVE_SPEED
  ), player.body.GetWorldCenter());
};

/**
 * @public
 */
YarnEntity.prototype.moveBackward = function() {
  this.body.ApplyImpulse(new Box2D.Common.Math.b2Vec2(
    -Math.cos(this.body.GetAngle()) * YarnEntity.MOVE_SPEED,
    -Math.sin(this.body.GetAngle()) * YarnEntity.MOVE_SPEED
  ), player.body.GetWorldCenter());
};

YarnEntity.prototype.moveToDestination = function() {

  if (this.destination) {

    // Check if destination was reached
    if (this.body.GetPosition().x > this.destination.x - 1 &&
      this.body.GetPosition().x < this.destination.x + 1 &&
      this.body.GetPosition().y > this.destination.y - 1 &&
      this.body.GetPosition().y < this.destination.y + 1) {
      this.destination = null;
      return false;
    }

    var diff = this.body.GetPosition().Copy();
    diff.Subtract(this.destination);
    // Get the angle of the destination
    var angle = Math.atan2(diff.y, diff.x) + (Math.PI);

    // Get the angle between the destination angle and the current angle
    angle -= (this.body.GetAngle() % (2 * Math.PI));
    angle %= Math.PI * 2;

    // Get the shortest version of the angle
    if (angle < -Math.PI) {
      angle += Math.PI;
      angle = -angle;
    } if (angle > Math.PI) {
      angle -= Math.PI;
      angle = -angle;
    }

    if (angle < 0) {
      this.rotateLeft();
    } else if (angle > 0) {
      this.rotateRight();
    }
    this.moveForward();
    return true;
  }

  return false;
};

YarnEntity.prototype.teleport = function(x, y) {
  this.body.SetPosition(new Box2D.Common.Math.b2Vec2(x / Yarn.SCALE, y / Yarn.SCALE));
};

YarnEntity.prototype.setDestination = function(x, y) {
  this.destination = new Box2D.Common.Math.b2Vec2(x / Yarn.SCALE, y / Yarn.SCALE);
};

function YarnHUDElement() {
  this.position = {};
  this.position.x = 0;
  this.position.y = 0;
}

YarnHUDElement.prototype.setImage = function(image) {
  this.bitmap = new createjs.Bitmap(image);

  // TODO: allow these parameters to be configured
  this.bitmap.regX = this.bitmap.regY = 32;
  this.bitmap.scaleX = 0.25;
  this.bitmap.scaleY = 0.25;
};

YarnHUDElement.prototype.setPosition = function(x, y) {
  this.position.x = x;
  this.position.y = y;
};